#!/bin/bash
date;
echo START;

###############
#check updates#
###############
cd ~;
sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt autoremove --purge -y && sudo apt clean;
./app_builds/android_sdk/cmdline-tools/latest/bin/sdkmanager --update;

######################
#check if needs build#
######################

cd ~/app_builds/fedilab;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/nitterizeme;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/davx5;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/organicmaps;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/vanilla;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/gpstest;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/aurorastore;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/smartcookieweb;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/ctemplar;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/librespeed/;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/fediphoto/;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/subz;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/k9mail;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/syncthing;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/skymap;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/pdfviewer;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/scrambledexif;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/vanillatag;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/vanillacover;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

###token genius error
#cd ~/app_builds/vanillalyrics;
#echo $PWD;
#if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
#else
#echo "Build it";
#echo 'build' $PWD >> ../tobuild;
#fi

cd ~/app_builds/vanillameta;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/tusky;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/keepassdx;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/nextcloud;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/seadroid;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/qrscanner;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

#cd ~/app_builds/spotiflyer;
#echo $PWD;
#if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
#else
#echo "Build it";
#echo 'build' $PWD >> ../tobuild;
#fi

cd ~/app_builds/termux;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/termuxapi;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/wireguard;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/corona;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/opentracks;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/imagepipe;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/acrylic;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/blabberim;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/osmdashboard;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/tutanota;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/streetcomplete;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/florisboard;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/netguard;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/towercollector;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/homeapp;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/revolutionirc;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/nextcloudnotes;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/phonesaver;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/carreport;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/shelter;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/foxydroid;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/openfoodfacts;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/editor;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/paintroid;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/opentasks;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/localgsm;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/gitnex;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/mozillanlp;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/nominatim;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/opentopomap;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/newpipe;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/nextcloudbookmarks;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/openkeychain;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/termbot;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/dvd;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/invizible;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/superfreeze;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/etar;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/grocy;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

################
#microg section#
################

###no apk output?###
cd ~/app_builds/unifiednlp;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/gmscore;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/gsfproxy;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/fakestore;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

cd ~/app_builds/droidguard;
echo $PWD;
if git pull --recurse-submodules | grep "Già aggiornato."; then echo "No changes - Skipping";
else
echo "Build it";
echo 'build' $PWD >> ../tobuild;
fi

################
#fdroid updates#
################

#pip3 install --upgrade fdroidserver;
cd ~/app_builds/_fdroidserver;
git pull;
#cd ~/app_builds/fdroid;
#git pull origin master;

date;
echo END UPDATE;