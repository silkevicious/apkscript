#!/bin/bash
date;
echo START;

###############
#check updates#
###############
cd ~;
#sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt autoremove --purge -y && sudo apt clean;
#./app_builds/android_sdk/cmdline-tools/latest/bin/sdkmanager --update;

#######
#build#
#######

cd ~/app_builds/fedilab;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/jcenter()/a\        google()' build.gradle;
sed -i -e '/playstoreImplementation/d' -e '/dl.bintray.com/d' -e 's/com.github.pengfeizhou.android.animation:glide-plugin:0.2.16/com.github.penfeizhou.APNG4Android:glide-plugin:0.2.8/' app/build.gradle;
./gradlew assembleFdroidRelease;
./gradlew assembleLiteRelease;
mv app/build/outputs/apk/fdroid/release/app-fdroid-release-unsigned.apk ../fr.gouv.etalab.mastodon_dev.apk;
mv app/build/outputs/apk/lite/release/app-lite-release-unsigned.apk ../app.fedilab.lite_dev.apk;
./gradlew -stop;

cd ~/app_builds/nitterizeme;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleFullLinksRelease;
./gradlew assembleLiteRelease;
mv app/build/outputs/apk/fullLinks/release/app-fullLinks-release-unsigned.apk ../app.fedilab.nitterizeme_dev.apk;
mv app/build/outputs/apk/lite/release/app-lite-release-unsigned.apk ../app.fedilab.nitterizemelite_dev.apk;
./gradlew -stop;

cd ~/app_builds/davx5;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e 's/com.google.android:flexbox/com.github.google:flexbox-layout/' app/build.gradle;
sed -i -e '/flexbox-layout/d' build.gradle;
./gradlew assembleStandardRelease;
mv app/build/outputs/apk/standard/release/app-standard-release-unsigned.apk ../at.bitfire.davdroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/organicmaps;
echo $PWD;
echo 'build' $PWD >> ../build;
cd ~/app_builds/organicmaps/android;
sed -i -e '/com.google.firebase/d; /com.google.gms/d; /com.google.android.gms/d' build.gradle;
touch secure.properties;
echo '<?xml version="1.0" encoding="utf-8"?><network-security-config/>' > res/xml/network_security_config.xml;
cp ../private_default.h ../private.h;
sed -i -e '/signingConfigs {/,+15d' build.gradle;
sed '/signingConfig signingConfigs./s/^/\/\//' -i build.gradle;
cd ../3party/boost;
./bootstrap.sh;
./b2 headers;
cd ~/app_builds/organicmaps/android;
./gradlew assembleFdroidRelease;
mv build/outputs/apk/fdroid/release/Organic* ../../app.organicmaps_dev.apk
./gradlew -stop;

cd ~/app_builds/vanilla;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../ch.blinkenlights.android.vanilla_dev.apk;
./gradlew -stop;

cd ~/app_builds/gpstest;
echo $PWD;
echo 'build' $PWD >> ../build;
#sed -i -e '/theTask.dependsOn "askForPasswords"/d' build.gradle;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i GPSTest/build.gradle;
./gradlew assembleOsmdroidRelease;
mv GPSTest/build/outputs/apk/osmdroid/release/osmdroidRelease* ../com.android.gpstest.osmdroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/aurorastore;
echo $PWD;
echo 'build' $PWD >> ../build;
#sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.aurora.store_dev.apk;
./gradlew -stop;

cd ~/app_builds/smartcookieweb;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-armeabi-v7a-release-unsigned.apk ../com.cookiejarapps.android.smartcookieweb_armv7a_dev.apk;
mv app/build/outputs/apk/release/app-arm64-v8a-release-unsigned.apk ../com.cookiejarapps.android.smartcookieweb_arm64_dev.apk;
./gradlew -stop;

cd ~/app_builds/ctemplar;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.ctemplar.app.fdroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/librespeed;
echo $PWD;
echo 'build' $PWD >> ../build;
cd ~/app_builds/librespeed/Speedtest-Android;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../../com.dosse.speedtest_dev.apk;
./gradlew -stop;

cd ~/app_builds/fediphoto;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.fediphoto.lineage_dev.apk;
./gradlew -stop;

cd ~/app_builds/subz;
echo $PWD;
echo 'build' $PWD >> ../build;
npm i;
ionic build --prod;
npx cap sync;
cd android;
chmod u+x gradlew;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release.apk ../../com.flasskamp.subz_dev.apk;
./gradlew -stop;

cd ~/app_builds/k9mail;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/k9mail/build/outputs/apk/release/k9mail-release-unsigned.apk ../com.fsck.k9_dev.apk;
./gradlew -stop;

cd ~/app_builds/syncthing;
echo $PWD;
echo 'build' $PWD >> ../build;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
cp ../davx5/gradle/wrapper/gradle-wrapper.jar gradle/wrapper/gradle-wrapper.jar;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.github.catfriend1.syncthingandroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/skymap;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/gms/d' build.gradle;
sed -i -e '/com.google.gms.google-services/d' app/build.gradle;
./gradlew assembleFdroidRelease;
mv app/build/outputs/apk/fdroid/release/app-fdroid-release-unsigned.apk ../com.google.android.stardroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/pdfviewer;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.gsnathan.pdfviewer_dev.apk;
./gradlew -stop;

cd ~/app_builds/scrambledexif;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleStandard;
mv app/build/outputs/apk/standard/release/scrambled*.apk ../com.jarsilio.android.scrambledeggsif_dev.apk;
./gradlew -stop;

cd ~/app_builds/vanillatag;
echo $PWD;
echo 'build' $PWD >> ../build;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.kanedias.vanilla.audiotag_dev.apk;
./gradlew -stop;

cd ~/app_builds/vanillacover;
echo $PWD;
echo 'build' $PWD >> ../build;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.kanedias.vanilla.coverfetch_dev.apk;
./gradlew -stop;

###token genius error
#cd ~/app_builds/vanillalyrics;
#echo $PWD;
#echo 'build' $PWD >> ../build;
#sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
#./gradlew assembleRelease;
#mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.kanedias.vanilla.lyrics_dev.apk;
#./gradlew -stop;

cd ~/app_builds/vanillameta;
echo $PWD;
echo 'build' $PWD >> ../build;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
sed -i -e '/splits/,+15d' app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.kanedias.vanilla.metadata_dev.apk;
./gradlew -stop;

cd ~/app_builds/keepassdx;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleLibreRelease;
mv app/build/outputs/apk/libre/release/app-libre-release-unsigned.apk ../com.kunzisoft.keepass.libre_dev.apk;
./gradlew -stop;

cd ~/app_builds/nextcloud;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleGeneric;
mv build/outputs/apk/generic/release/generic-release-*.apk ../com.nextcloud.client_dev.apk;
./gradlew -stop;

cd ~/app_builds/seadroid;
echo $PWD;
echo 'build' $PWD >> ../build;
cp app/key.properties.example app/key.properties;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/seafile*.apk ../com.seafile.seadroid2_dev.apk;
./gradlew -stop;

cd ~/app_builds/qrscanner;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/maven {/,+2d' build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.secuso.privacyFriendlyCodeScanner_dev.apk;
./gradlew -stop;

#cd ~/app_builds/spotiflyer;
#echo $PWD;
#echo 'build' $PWD >> ../build;
#find ffmpeg/ -type f -name "*.sh" -exec dos2unix {} \+
#find ffmpeg/ -type f -name "*.sh" -exec chmod u+x {} \+
#./ffmpeg/ffmpeg-android-maker/ffmpeg-android-maker.sh --android-api-level=21 -lame
#./ffmpeg/copy-ffmpeg-executables.sh
#./gradlew assembleRelease;
#mv android/build/outputs/apk/release/android-release-unsigned.apk ../com.shabinder.spotiflyer_dev.apk;
#./gradlew -stop;

cd ~/app_builds/termux;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/splits\ {/,+7d' app/build.gradle;
#universalApk true
#sed -i -e '/publishing {/,/^}/d' terminal-{emulator,view}/build.gradle;
#sed -i -e '/publishing {/,/^}/d' termux-shared/build.gradle;
sed -i -e 's/22.0.7026061/22.1.7171670/' gradle.properties;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.termux_dev.apk;
./gradlew -stop;

cd ~/app_builds/termuxapi;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.termux.api_dev.apk;
./gradlew -stop;

cd ~/app_builds/wireguard;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/publish.gradle/d' tunnel/build.gradle;
./gradlew assembleRelease;
mv ui/build/outputs/apk/release/ui-release-unsigned.apk ../com.wireguard.android_dev.apk;
./gradlew -stop;

cd ~/app_builds/opentracks;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv build/outputs/apk/release/opentracks-release-unsigned.apk ../de.dennisguse.opentracks_dev.apk;
./gradlew -stop;

cd ~/app_builds/corona;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleDeviceRelease;
mv Corona-Warn-App/build/outputs/apk/device/release/Corona-Warn-App-device-release-unsigned.apk ../de.corona.tracing_dev.apk;
./gradlew -stop;

cd ~/app_builds/imagepipe;
echo $PWD;
echo 'build' $PWD >> ../build;
cp ../localgsm/gradle/wrapper/gradle-wrapper.jar gradle/wrapper/gradle-wrapper.jar;
cp ../localgsm/gradle/wrapper/gradle-wrapper.properties gradle/wrapper/gradle-wrapper.properties;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../de.kaffeemitkoffein.imagepipe_dev.apk;
./gradlew -stop;

cd ~/app_builds/acrylic;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assemble;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../de.moooon.acrylicons_dev.apk;
./gradlew -stop;

cd ~/app_builds/blabberim;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/libwebrtc/d' -e "/AppIntro:/aimplementation 'org.webrtc:google-webrtc:1.0.30039'" build.gradle;
./gradlew assembleGitRelease;
mv build/outputs/apk/git/release/blabber*.apk ../de.pixart.messenger_dev.apk;
./gradlew -stop;

cd ~/app_builds/osmdashboard;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleFullRelease;
./gradlew assembleOfflineRelease;
mv build/outputs/apk/full/release/OpenTracksOSMPlugIn-full-release-unsigned.apk ../de.storchp.opentracks.osmplugin_dev.apk;
mv build/outputs/apk/offline/release/OpenTracksOSMPlugIn-offline-release-unsigned.apk ../de.storchp.opentracks.osmplugin.offline_dev.apk;
./gradlew -stop;

cd ~/app_builds/tutanota;
echo $PWD;
echo 'build' $PWD >> ../build;
node buildSrc/fixFdroidDeps.js
npm install
node dist prod
node buildSrc/prepareMobileBuild.js dist
cd app-android;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleFdroidRelease;
mv app/build/outputs/apk/fdroid/release/tuta* ../../de.tutao.tutanota_dev.apk;
./gradlew -stop;

cd ~/app_builds/streetcomplete;
echo $PWD;
echo 'build' $PWD >> ../build;
sed '/signingConfig = signingConfigs.getByName/s/^/\/\//' -i app/build.gradle.kts;
sed -i -e '/keystorePropertiesFile.*{/,/}/d; /keystorePropertiesFile/d' app/build.gradle.kts;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../de.westnordost.streetcomplete_dev.apk;
./gradlew -stop;

cd ~/app_builds/florisboard;
echo $PWD;
echo 'build' $PWD >> ../build;
NDK=$ANDROID_HOME/ndk/22.1.7171670/ SDK=$ANDROID_HOME bash app/src/main/icu4c/floris-cc-icu4c.sh
./gradlew assembleBeta;
mv app/build/outputs/apk/beta/app-beta-unsigned.apk ../dev.patrickgold.florisboard_dev.apk;
./gradlew -stop;

cd ~/app_builds/netguard;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/keystore/d' app/build.gradle;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/NetGuard* ../eu.faircode.netguard_dev.apk;
./gradlew -stop;

cd ~/app_builds/towercollector;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
cp app/properties/private-fdroid.properties app/properties/private.properties;
#commentato a mano altre cose
#provato con build
./gradlew assembleproductionFdroidRelease;
mv app/build/outputs/apk/productionFdroid/release/*.apk ../info.zamojski.soft.towercollector_dev.apk;
./gradlew -stop;

cd ~/app_builds/homeapp;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../io.github.domi04151309.home_dev.apk;
./gradlew -stop;

cd ~/app_builds/revolutionirc;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../io.mrarm.irc_dev.apk;
./gradlew -stop;

cd ~/app_builds/nextcloudnotes;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleFdroid;
mv app/build/outputs/apk/fdroid/release/app-fdroid-release-unsigned.apk ../it.niedermann.owncloud.notes_dev.apk;
./gradlew -stop;

cd ~/app_builds/phonesaver;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../link.standen.michael.phonesaver_dev.apk;
./gradlew -stop;

cd ~/app_builds/carreport;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
sed -i -e '/fullImplementation.* {/,/}/d' -e '/fullImplementation /d' app/build.gradle;
./gradlew assembleFossRelease;
mv app/build/outputs/apk/foss/release/app-foss-release-unsigned.apk ../me.kuehle.carreport_dev.apk;
./gradlew -stop;

cd ~/app_builds/shelter;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../net.typeblog.shelter_dev.apk;
./gradlew -stop;

cd ~/app_builds/foxydroid;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv build/outputs/apk/release/foxy*.apk ../nya.kitsunyan.foxydroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/openfoodfacts;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i 's/\r$//' gradle/wrapper/gradle-wrapper.properties;
sed -i '/sentry-android-gradle-plugin/d' build.gradle.kts;
sed -i -e '/javaMaxHeapSize/d' -e '/io.sentry.android.gradle/d' app/build.gradle.kts;
sed '/signingConfig = signingConfigs.getByName/s/^/\/\//' -i app/build.gradle.kts;
./gradlew assembleOffFdroidRelease;
./gradlew assembleObfFdroidRelease;
./gradlew assembleOpfFdroidRelease;
./gradlew assembleOpffFdroidRelease;
mv app/build/outputs/apk/offFdroid/release/app-off-fdroid-release-unsigned.apk ../openfoodfacts.github.scrachx.openfood_dev.apk;
mv app/build/outputs/apk/obfFdroid/release/app-obf-fdroid-release-unsigned.apk ../openfoodfacts.github.scrachx.openbeauty_dev.apk
mv app/build/outputs/apk/opfFdroid/release/app-opf-fdroid-release-unsigned.apk ../org.openproductsfacts.scanner_dev.apk
mv app/build/outputs/apk/opffFdroid/release/app-opff-fdroid-release-unsigned.apk ../org.openpetfoodfacts.scanner_dev.apk
./gradlew -stop;

cd ~/app_builds/editor;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv build/outputs/apk/release/*.apk ../org.billthefarmer.editor_dev.apk;
./gradlew -stop;

cd ~/app_builds/paintroid;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/*.apk ../org.catrobat.paintroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/opentasks;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv opentasks/build/outputs/apk/release/*.apk ../org.dmfs.tasks_dev.apk;
./gradlew -stop;

cd ~/app_builds/localgsm;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/*.apk ../org.fitchfamily.android.gsmlocation_dev.apk;
./gradlew -stop;

cd ~/app_builds/gitnex;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleFreeRelease;
mv app/build/outputs/apk/free/release/*.apk ../org.mian.gitnex_dev.apk;
./gradlew -stop;

cd ~/app_builds/mozillanlp;
echo $PWD;
echo 'build' $PWD >> ../build;
#add permission in src/main/AndroidManifest.xml
#<uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION"/>
#sed -i '/FINE_LOCATION\" \/>/s/^/\/\//' 
./gradlew assembleRelease;
mv build/outputs/apk/release/*.apk ../org.microg.nlp.backend.ichnaea_dev.apk;
./gradlew -stop;

cd ~/app_builds/nominatim;
echo $PWD;
echo 'build' $PWD >> ../build;
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64;
./gradlew assembleRelease;
mv build/outputs/apk/*.apk ../org.microg.nlp.backend.nominatim_dev.apk;
./gradlew -stop;
JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64;

cd ~/app_builds/opentopomap;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../org.nitri.opentopo_dev.apk;
./gradlew -stop;

cd ~/app_builds/newpipe;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/*.apk ../org.schabi.newpipe_dev.apk;
./gradlew -stop;

cd ~/app_builds/nextcloudbookmarks;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e 's/applicationId "org.bisw.nxbookmarks"/applicationId "org.schabi.nxbookmarks"/g' app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/fdroid/release/*.apk ../org.schabi.nxbookmarks_dev.apk;
./gradlew -stop;

cd ~/app_builds/openkeychain;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleFdroidRelease;
mv OpenKeychain/build/outputs/apk/fdroid/release/*.apk ../org.sufficientlysecure.keychain_dev.apk;
./gradlew -stop;

cd ~/app_builds/termbot;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/googleImplementation/d' app/build.gradle;
sed -i -e 's/21.3.6528147/21.4.7075529/' app/build.gradle;
sed -i -e 's|url MAVEN_REPO_CACHE|url "https://jitpack.io"|' build.gradle;
sed -i -e 's|url MAVEN_REPO_CACHE|url "https://jitpack.io"|' settings.gradle;
./gradlew assembleOssRelease;
mv app/build/outputs/apk/oss/release/*.apk ../org.sufficientlysecure.termbot_dev.apk;
./gradlew -stop;

cd ~/app_builds/dvd;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-arm64-v8a-release-unsigned.apk ../org.yausername.dvd_arm64-v8a_dev.apk;
mv app/build/outputs/apk/release/app-armeabi-v7a-release-unsigned.apk ../org.yausername.dvd_armeabi-v7a_dev.apk;
./gradlew -stop;

cd ~/app_builds/invizible;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e "s/ndkVersion .*/ndkVersion '20.1.5948944'/" tordnscrypt/build.gradle
cp ../davx5/gradle/wrapper/gradle-wrapper.jar gradle/wrapper/gradle-wrapper.jar;
chmod u+x gradlew;
./gradlew assembleFdroidArmv7aRelease;
./gradlew assembleFdroidArm64Release;
mv tordnscrypt/build/outputs/apk/fdroidArmv7a/release/tordnscrypt-fdroid-armv7a-release-unsigned.apk ../pan.alexander.tordnscrypt.stable_armv7a_dev.apk;
mv tordnscrypt/build/outputs/apk/fdroidArm64/release/tordnscrypt-fdroid-arm64-release-unsigned.apk ../pan.alexander.tordnscrypt.stable_arm64_dev.apk;
./gradlew -stop;

cd ~/app_builds/superfreeze;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv build/outputs/apk/release/*.apk ../superfreeze.tool.android_dev.apk;
./gradlew -stop;

cd ~/app_builds/etar;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv build/outputs/apk/release/*.apk ../ws.xsoh.etar_dev.apk;
./gradlew -stop;

cd ~/app_builds/grocy;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../xyz.zedler.patrick.grocy_dev.apk;
./gradlew -stop;

################
#microg section#
################

###no apk output?###
cd ~/app_builds/unifiednlp;
echo $PWD;
echo 'build' $PWD >> ../build;
find . -type f -print0 -name build.gradle | xargs -0 sed -i -e '/com.github.dcendents/d';
./gradlew assemble;
mv app/build/outputs/apk/NetworkLocation/release/*.apk ../org.microg.nlp_dev.apk;
./gradlew -stop;

cd ~/app_builds/gmscore;
echo $PWD;
echo 'build' $PWD >> ../build;
#./gradlew assembleWithMapboxWithNearbyRelease;
#mv play-services-core/build/outputs/apk/withMapboxWithNearby/release/*.apk ../com.google.android.gms_dev.apk;
./gradlew assembleWithVtmWithNearbyRelease;
mv play-services-core/build/outputs/apk/withVtmWithNearby/release/*.apk ../com.google.android.gms_dev.apk;
./gradlew -stop;

cd ~/app_builds/gsfproxy;
echo $PWD;
echo 'build' $PWD >> ../build;
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64;
./gradlew build;
mv services-framework-proxy/build/outputs/apk/services-framework-proxy-r*.apk ../com.google.android.gsf_dev.apk;
./gradlew -stop;
JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64;

cd ~/app_builds/fakestore;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew build;
mv fake-store/build/outputs/apk/release/*.apk ../com.android.vending_dev.apk;
./gradlew -stop;

cd ~/app_builds/droidguard;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew build;
mv app/build/outputs/apk/release/*.apk ../org.microg.gms.droidguard_dev.apk;
./gradlew -stop;

mv ~/app_builds/*.apk ~/app_builds/fdroid/unsigned/;

################
#fdroid updates#
################

cd ~/app_builds/_fdroidserver;
git pull;
#cd ~/app_builds/fdroid;
#git pull origin master;

date;
echo END BUILDING;

sudo shutdown -h now;