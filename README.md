# apkscript

![License](https://img.shields.io/badge/license-WTFPL-green.svg)

Scripts to manage an F-Droid compatible repository.

Feel free to contribute, fork, share, whatever, see LICENSE.