#!/bin/bash
date;
echo START;

###############
#check updates#
###############
cd ~;
sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt autoremove --purge -y && sudo apt clean;
./app_builds/android_sdk/cmdline-tools/latest/bin/sdkmanager --update;

#######
#clean#
#######

cd ~/app_builds/fedilab;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/nitterizeme;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/davx5;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/organicmaps/android;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/vanilla;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/gpstest;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/aurorastore;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/smartcookieweb;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/ctemplar;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/librespeed/Speedtest-Android;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/fediphoto/;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/subz/android;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/k9mail;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/syncthing;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/skymap;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/pdfviewer;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/scrambledexif;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/vanillatag;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/vanillacover;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

###token genius error
#cd ~/app_builds/vanillalyrics;
#echo $PWD;
#./gradlew clean;
#./gradlew cleanBuildCache;
#./gradlew -stop;

cd ~/app_builds/vanillameta;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/tusky;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/keepassdx;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/nextcloud;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/seadroid;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/qrscanner;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

#cd ~/app_builds/spotiflyer;
#echo $PWD;
#./gradlew clean;
#./gradlew cleanBuildCache;
#./gradlew -stop;

cd ~/app_builds/termux;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/termuxapi;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/wireguard;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/corona;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/opentracks;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/imagepipe;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/acrylic;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/blabberim;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/osmdashboard;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/tutanota/app-android;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/streetcomplete;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/florisboard;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/netguard;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/towercollector;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/homeapp;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/revolutionirc;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/nextcloudnotes;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/phonesaver;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/carreport;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/shelter;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/foxydroid;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/openfoodfacts;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/editor;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/paintroid;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/opentasks;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/localgsm;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/gitnex;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/mozillanlp;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/nominatim;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/opentopomap;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/newpipe;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/nextcloudbookmarks;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/openkeychain;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/termbot;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/dvd;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/invizible;
echo $PWD;
./gradlew clean;
./gradlew -stop;

cd ~/app_builds/superfreeze;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/etar;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/grocy;
echo $PWD;
./gradlew clean;
./gradlew -stop;

################
#microg section#
################

###no apk output?###
cd ~/app_builds/unifiednlp;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/gmscore;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/gsfproxy;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/fakestore;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

cd ~/app_builds/droidguard;
echo $PWD;
./gradlew clean;
./gradlew cleanBuildCache;
./gradlew -stop;

##############
#gradle clean#
##############

pkill -f '.*GradleDaemon.*';
rm -rf ~/.gradle/caches/*;

################
#fdroid updates#
################

#pip3 install --upgrade fdroidserver;
cd ~/app_builds/_fdroidserver;
git pull;
#cd ~/app_builds/fdroid;
#git pull origin master;

date;
echo END UPDATE;