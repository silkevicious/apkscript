#!/bin/bash
date;
echo START; cd ~;
##########
#main dep#
##########
sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt autoremove --purge -y && sudo apt clean;
sudo apt install git wget unzip default-jdk-headless screen -y;
sudo sh -c "echo 'deb http://deb.debian.org/debian/ stretch main' >> /etc/apt/sources.list.d/stretch.list";
sudo sh -c "echo 'deb http://deb.debian.org/debian experimental main' >> /etc/apt/sources.list.d/experimental.list";
sudo sh -c "echo '\ndeb http://security.debian.org/debian-security/ stretch/updates main' >> /etc/apt/sources.list.d/stretch.list";
sudo sh -c "printf 'Package: *\nPin: release a=stretch\nPin-Priority: 90\n' >> /etc/apt/preferences.d/limit-stretch";
sudo sh -c "printf 'Package: *\nPin: release a=experimental\nPin-Priority: 80\n' >> /etc/apt/preferences.d/limit-experimental";
sudo apt update && sudo apt install openjdk-8-jdk-headless -y;

###############
#android setup#
###############
mkdir app_builds;
cd app_builds;
wget https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip -O sdk.zip;
mkdir -p android_sdk/cmdline-tools/;
unzip sdk.zip -d android_sdk/cmdline-tools;
mv android_sdk/cmdline-tools/cmdline-tools android_sdk/cmdline-tools/latest;
rm sdk.zip;
echo "export ANDROID_HOME=/home/debian/app_builds/android_sdk" >> ~/.bashrc;
source ~/.bashrc;
echo "export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64" >> ~/.profile;
echo "export PATH=\$PATH:\$JAVA_HOME/bin" >> ~/.profile;
source ~/.profile;

./android_sdk/cmdline-tools/latest/bin/sdkmanager --licenses;
./android_sdk/cmdline-tools/latest/bin/sdkmanager --install "build-tools;31.0.0";
./android_sdk/cmdline-tools/latest/bin/sdkmanager --install "ndk;21.3.6528147"; #vanillameta
./android_sdk/cmdline-tools/latest/bin/sdkmanager --install "ndk;21.0.6113669"; #dvd
./android_sdk/cmdline-tools/latest/bin/sdkmanager --install "cmake;3.18.1"; #organicmaps
#############
#general dep#
#############
sudo apt install curl gradle maven golang-go python-is-python3 python-dev-is-python3 npm autogen -y;
sudo apt install python3-pip curl rsync -y;
sudo npm install -g @ionic/cli; #subz dep
sudo npm install commander; #tuta?
sudo npm install ospec; #tuta?
sudo apt install -t experimental nodejs;
#apt-get install -y ninja-build dos2unix autoconf automake build-essential libtool pkg-config yasm cmake meson nasm; #spotiflyer
##############
#gradle setup#
##############
mkdir -p ~/.gradle/;
touch ~/.gradle/gradle.properties;
echo "org.gradle.jvmargs=-Xmx9g -XX:MaxMetaspaceSize=2048m -XX:+HeapDumpOnOutOfMemoryError" >> ~/.gradle/gradle.properties;
#############
#repos setup#
#############
cd ~/app_builds/;
git clone --single-branch --recurse-submodules https://framagit.org/tom79/fedilab.git fedilab;
git clone --single-branch --recurse-submodules https://framagit.org/tom79/nitterizeme.git nitterizeme;
git clone --single-branch --recurse-submodules https://gitlab.com/bitfireAT/davx5-ose.git davx5;
git clone --single-branch --recurse-submodules https://github.com/organicmaps/organicmaps organicmaps;
git clone --single-branch --recurse-submodules https://github.com/vanilla-music/vanilla.git vanilla;
git clone --single-branch --recurse-submodules https://github.com/barbeau/gpstest gpstest;
git clone --single-branch --recurse-submodules https://gitlab.com/AuroraOSS/AuroraStore.git aurorastore;
git clone --single-branch --recurse-submodules https://github.com/CookieJarApps/SmartCookieWeb-Preview.git smartcookieweb;
cd smartcookieweb && chmod u+x gradlew && cd ../ ;
git clone --recurse-submodules https://github.com/CTemplar/android.git --single-branch --branch fdroid-dev ctemplar;
cd ctemplar && chmod u+x gradlew && cd ../ ;
git clone --recurse-submodules https://github.com/librespeed/speedtest-android --single-branch --branch mydemo librespeed;
git clone --recurse-submodules https://codeberg.org/silkevicious/fediphoto-lineage fediphoto;
git clone --single-branch --recurse-submodules https://codeberg.org/epinez/Subz/ subz;
cd subz/android && chmod u+x gradlew && cd ../../ ;
git clone --single-branch --recurse-submodules https://github.com/k9mail/k-9 k9mail;
git clone --single-branch --recurse-submodules https://github.com/Catfriend1/syncthing-android-fdroid.git syncthing;
git clone --single-branch --recurse-submodules https://github.com/sky-map-team/stardroid skymap;
git clone --single-branch --recurse-submodules https://github.com/JavaCafe01/PdfViewer pdfviewer;
git clone --single-branch --recurse-submodules https://gitlab.com/juanitobananas/scrambled-exif.git scrambledexif;
git clone --single-branch --recurse-submodules https://github.com/vanilla-music/vanilla-music-tag-editor vanillatag;
git clone --single-branch --recurse-submodules https://github.com/vanilla-music/vanilla-music-cover-fetch vanillacover;
#git clone --single-branch --recurse-submodules https://github.com/vanilla-music/vanilla-music-lyrics-search vanillalyrics;
git clone --single-branch --recurse-submodules https://github.com/vanilla-music/vanilla-music-metadata-fetch vanillameta;
git clone --single-branch --recurse-submodules https://github.com/tuskyapp/Tusky.git tusky;
git clone --single-branch --recurse-submodules https://github.com/Kunzisoft/KeePassDX keepassdx;
git clone --single-branch --recurse-submodules https://github.com/nextcloud/android.git nextcloud;
git clone --single-branch --recurse-submodules https://github.com/haiwen/seadroid.git seadroid;
cp seadroid/app/key.properties.example seadroid/app/key.properties;
git clone --single-branch --recurse-submodules https://github.com/SecUSo/privacy-friendly-qr-scanner qrscanner;
#git clone --single-branch --recurse-submodules https://github.com/Shabinder/SpotiFlyer spotiflyer;
git clone --single-branch --recurse-submodules https://github.com/termux/termux-app termux;
git clone --single-branch --recurse-submodules https://github.com/termux/termux-api termuxapi;
git clone --single-branch --recurse-submodules https://git.zx2c4.com/wireguard-android/ wireguard;
git clone --single-branch --recurse-submodules https://codeberg.org/corona-contact-tracing-germany/cwa-android corona;
git clone --single-branch --recurse-submodules https://github.com/OpenTracksApp/OpenTracks opentracks;
git clone --single-branch --recurse-submodules https://codeberg.org/Starfish/Imagepipe.git imagepipe;
git clone --single-branch --recurse-submodules https://codeberg.org/mondstern/AndroidAcrylicIconPack acrylic;
git clone --single-branch --recurse-submodules https://codeberg.org/kriztan/blabber.im.git blabberim;
git clone --single-branch --recurse-submodules https://github.com/OpenTracksApp/OSMDashboard osmdashboard;
git clone --single-branch --recurse-submodules https://github.com/tutao/tutanota.git tutanota;
git clone --single-branch --recurse-submodules https://github.com/streetcomplete/StreetComplete.git streetcomplete;
git clone --single-branch --recurse-submodules https://github.com/florisboard/florisboard florisboard;
git clone --single-branch --recurse-submodules https://github.com/M66B/NetGuard netguard;
git clone --single-branch --recurse-submodules https://github.com/zamojski/TowerCollector towercollector;
cd towercollector && chmod u+x gradlew && cd ../ ;
git clone --single-branch --recurse-submodules https://github.com/Domi04151309/HomeApp homeapp;
cd homeapp && chmod u+x gradlew && cd ../ ;
git clone --single-branch --recurse-submodules https://github.com/MCMrARM/revolution-irc.git revolutionirc;
git clone --single-branch --recurse-submodules https://github.com/stefan-niedermann/nextcloud-notes.git nextcloudnotes;
git clone --single-branch --recurse-submodules https://github.com/ScreamingHawk/phone-saver phonesaver;
cd phonesaver && chmod u+x gradlew && cd ../ ;
git clone --single-branch --recurse-submodules https://bitbucket.org/frigus02/car-report.git carreport;
cd carreport && chmod u+x gradlew && cd ../ ;
git clone --single-branch --recurse-submodules https://github.com/PeterCxy/Shelter.git shelter;
git clone --single-branch --recurse-submodules https://github.com/kitsunyan/foxy-droid foxydroid;
git clone --single-branch --recurse-submodules https://github.com/openfoodfacts/openfoodfacts-androidapp openfoodfacts;
git clone --single-branch --recurse-submodules https://github.com/billthefarmer/editor editor;
git clone --single-branch --recurse-submodules https://github.com/Catrobat/Paintroid paintroid;
git clone --single-branch --recurse-submodules https://github.com/dmfs/opentasks.git opentasks;
git clone --single-branch --recurse-submodules https://gitlab.com/deveee/Local-GSM-Backend.git localgsm;
git clone --single-branch --recurse-submodules https://codeberg.org/gitnex/GitNex.git gitnex;
git clone --single-branch --recurse-submodules https://github.com/microg/IchnaeaNlpBackend mozillanlp;
git clone --single-branch --recurse-submodules https://github.com/microg/NominatimGeocoderBackend nominatim; #8
git clone --single-branch --recurse-submodules https://github.com/Pygmalion69/OpenTopoMapViewer opentopomap;
git clone --single-branch --recurse-submodules https://github.com/TeamNewPipe/NewPipe newpipe;
git clone --single-branch --recurse-submodules https://gitlab.com/bisada/OCBookmarks.git nextcloudbookmarks;
git clone --single-branch --recurse-submodules https://github.com/open-keychain/open-keychain.git openkeychain;
git clone --single-branch --recurse-submodules https://github.com/cotechde/termbot termbot;
git clone --single-branch --recurse-submodules https://github.com/yausername/dvd dvd;
git clone --single-branch --recurse-submodules https://github.com/Gedsh/InviZible.git invizible;
cd invizible && chmod u+x gradlew && cd ../ ;
git clone --single-branch --recurse-submodules https://gitlab.com/SuperFreezZ/SuperFreezZ.git superfreeze;
git clone --single-branch --recurse-submodules https://github.com/Etar-Group/Etar-Calendar.git etar;
git clone --single-branch --recurse-submodules https://github.com/patzly/grocy-android.git grocy;
################
#microg section#
################
git clone --single-branch --recurse-submodules https://github.com/microg/UnifiedNlp unifiednlp;
git clone --single-branch --recurse-submodules https://github.com/microg/GmsCore gmscore; 
git clone --single-branch --recurse-submodules https://github.com/microg/android_packages_apps_GsfProxy	gsfproxy; #8
git clone --single-branch --recurse-submodules https://github.com/microg/FakeStore fakestore;
git clone --single-branch --recurse-submodules https://github.com/microg/RemoteDroidGuard droidguard;
###############
#sync metadata#
###############
mkdir fdroid;
cd fdroid;
#git init;
#git remote add -f origin https://gitlab.com/fdroid/fdroiddata;
#git config core.sparseCheckout true;
#echo "metadata/" >> .git/info/sparse-checkout;
#echo "srclibs/" >> .git/info/sparse-checkout;
#git pull origin master;
echo 'alias apksigner="~/app_builds/android_sdk/build-tools/31.0.0/apksigner sign --ks ~/app_builds/fdroid/keystore.p12 --ks-key-alias KEYSTOREALIAS"' >> ~/.bashrc;
source ~/.bashrc;
##############
#fdroid setup#
##############
pip3 install --upgrade pip;
cd ~/app_builds;
git clone https://gitlab.com/fdroid/fdroidserver.git _fdroidserver;
sudo pip3 install -e _fdroidserver;
#pip3 install --upgrade fdroidserver;
cd fdroid;
fdroid;
source ~/.profile;
fdroid init;
#asked for path /home/debian/app_builds/android_sdk
rm keystore.p12 config.yml;
rm -rf repo;
cp -r /mnt/shared/apk/fdroid/* . ;
mv apk repo;
mkdir unsigned;
mkdir apk;
mkdir metadata;
mkdir git-mirror;
chmod 0600 config.yml;
chmod 0600 keystore.p12;
###############
#ssh-git setup#
###############
cd ~;
echo 'SIGNPWD="SIGNINGPASSWORD"' >> ~/.bashrc; #SIGNINGPASSWORD
source ~/.bashrc;
eval "$(ssh-agent -s)";
ssh-keygen;
cp /mnt/shared/apk/gitkey* ~/.ssh/;
cd ~/.ssh/;
chmod 0600 gitkey*;
chown debian gitkey*;
git config --global user.name "YOUR NAME"; #GIT NAME
git config --global user.email "YOUR@E.MAIL"; #GIT MAIL
git config --global pull.rebase false;
mkdir -p ~/app_builds/fdroid/reset;
cd ~/app_builds/fdroid/reset;
git init;
git commit --allow-empty -m 'Reset';
git remote add origin git@codeberg.org:silkevicious/apkrepo.git;
cd ../;
ssh-add ~/.ssh/gitkey;
git clone git@codeberg.org:silkevicious/apkrepo.git test; #ACCEPT FINGERPRINT
date;
echo END SETUP;