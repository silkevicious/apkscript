#!/bin/bash
date;
echo START;
eval "$(ssh-agent -s)"; ssh-add ~/.ssh/gitkey;

#########
#prepare#
#########

cd ~/app_builds/fdroid; mv repo/*.apk apk; rm -rf repo git-mirror metadata; mkdir repo git-mirror metadata; cp README.md git-mirror; cp LICENSE.md git-mirror; cd ~/app_builds/fdroid/unsigned; for i in *.apk; do echo $SIGNPWD | apksigner $i; done; rm *.idsig; mv *.apk ../apk; cd ../; mv apk/*.apk repo; fdroid update -c; fdroid update; cd reset; git push --force --set-upstream origin master; cd ../; fdroid deploy -v;

date;
echo END DEPLOY;

