#!/bin/bash
date;
echo START;

###############
#check updates#
###############
cd ~;
#sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt autoremove --purge -y && sudo apt clean;
#./app_builds/android_sdk/cmdline-tools/latest/bin/sdkmanager --update;

#######
#build#
#######

cd ~/app_builds/davx5;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e 's/com.google.android:flexbox/com.github.google:flexbox-layout/' app/build.gradle;
sed -i -e '/flexbox-layout/d' build.gradle;
./gradlew assembleStandardRelease;
mv app/build/outputs/apk/standard/release/app-standard-release-unsigned.apk ../at.bitfire.davdroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/vanilla;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../ch.blinkenlights.android.vanilla_dev.apk;
./gradlew -stop;

cd ~/app_builds/aurorastore;
echo $PWD;
echo 'build' $PWD >> ../build;
#sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.aurora.store_dev.apk;
./gradlew -stop;

cd ~/app_builds/smartcookieweb;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-armeabi-v7a-release-unsigned.apk ../com.cookiejarapps.android.smartcookieweb_armv7a_dev.apk;
mv app/build/outputs/apk/release/app-arm64-v8a-release-unsigned.apk ../com.cookiejarapps.android.smartcookieweb_arm64_dev.apk;
./gradlew -stop;

cd ~/app_builds/ctemplar;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.ctemplar.app.fdroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/fediphoto;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.fediphoto.lineage_dev.apk;
./gradlew -stop;

cd ~/app_builds/k9mail;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/k9mail/build/outputs/apk/release/k9mail-release-unsigned.apk ../com.fsck.k9_dev.apk;
./gradlew -stop;

cd ~/app_builds/pdfviewer;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.gsnathan.pdfviewer_dev.apk;
./gradlew -stop;

cd ~/app_builds/vanillatag;
echo $PWD;
echo 'build' $PWD >> ../build;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.kanedias.vanilla.audiotag_dev.apk;
./gradlew -stop;

cd ~/app_builds/vanillacover;
echo $PWD;
echo 'build' $PWD >> ../build;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.kanedias.vanilla.coverfetch_dev.apk;
./gradlew -stop;

cd ~/app_builds/vanillameta;
echo $PWD;
echo 'build' $PWD >> ../build;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
sed -i -e '/splits/,+15d' app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.kanedias.vanilla.metadata_dev.apk;
./gradlew -stop;

cd ~/app_builds/tusky;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleGreenRelease;
mv app/build/outputs/apk/green/release/app-green-release-unsigned.apk ../com.keylesspalace.tusky_dev.apk;
./gradlew -stop;

cd ~/app_builds/keepassdx;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleLibreRelease;
mv app/build/outputs/apk/libre/release/app-libre-release-unsigned.apk ../com.kunzisoft.keepass.libre_dev.apk;
./gradlew -stop;

cd ~/app_builds/seadroid;
echo $PWD;
echo 'build' $PWD >> ../build;
cp app/key.properties.example app/key.properties;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/seafile*.apk ../com.seafile.seadroid2_dev.apk;
./gradlew -stop;

cd ~/app_builds/qrscanner;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/maven {/,+2d' build.gradle;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../com.secuso.privacyFriendlyCodeScanner_dev.apk;
./gradlew -stop;

cd ~/app_builds/wireguard;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/publish.gradle/d' tunnel/build.gradle;
./gradlew assembleRelease;
mv ui/build/outputs/apk/release/ui-release-unsigned.apk ../com.wireguard.android_dev.apk;
./gradlew -stop;

cd ~/app_builds/corona;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleDeviceRelease;
mv Corona-Warn-App/build/outputs/apk/device/release/Corona-Warn-App-device-release-unsigned.apk ../de.corona.tracing_dev.apk;
./gradlew -stop;

cd ~/app_builds/imagepipe;
echo $PWD;
echo 'build' $PWD >> ../build;
cp ../localgsm/gradle/wrapper/gradle-wrapper.jar gradle/wrapper/gradle-wrapper.jar;
cp ../localgsm/gradle/wrapper/gradle-wrapper.properties gradle/wrapper/gradle-wrapper.properties;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../de.kaffeemitkoffein.imagepipe_dev.apk;
./gradlew -stop;

cd ~/app_builds/blabberim;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/libwebrtc/d' -e "/AppIntro:/aimplementation 'org.webrtc:google-webrtc:1.0.30039'" build.gradle;
./gradlew assembleGitRelease;
mv build/outputs/apk/git/release/blabber*.apk ../de.pixart.messenger_dev.apk;
./gradlew -stop;

cd ~/app_builds/tutanota;
echo $PWD;
echo 'build' $PWD >> ../build;
node buildSrc/fixFdroidDeps.js
npm install
node dist prod
node buildSrc/prepareMobileBuild.js dist
cd app-android;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
./gradlew assembleFdroidRelease;
mv app/build/outputs/apk/fdroid/release/tuta* ../../de.tutao.tutanota_dev.apk;
./gradlew -stop;

cd ~/app_builds/streetcomplete;
echo $PWD;
echo 'build' $PWD >> ../build;
sed '/signingConfig = signingConfigs.getByName/s/^/\/\//' -i app/build.gradle.kts;
sed -i -e '/keystorePropertiesFile.*{/,/}/d; /keystorePropertiesFile/d' app/build.gradle.kts;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../de.westnordost.streetcomplete_dev.apk;
./gradlew -stop;

cd ~/app_builds/florisboard;
echo $PWD;
echo 'build' $PWD >> ../build;
NDK=$ANDROID_HOME/ndk/22.1.7171670/ SDK=$ANDROID_HOME bash app/src/main/icu4c/floris-cc-icu4c.sh
./gradlew assembleBeta;
mv app/build/outputs/apk/beta/app-beta-unsigned.apk ../dev.patrickgold.florisboard_dev.apk;
./gradlew -stop;

cd ~/app_builds/towercollector;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
cp app/properties/private-fdroid.properties app/properties/private.properties;
#commentato a mano altre cose
#provato con build
./gradlew assembleproductionFdroidRelease;
mv app/build/outputs/apk/productionFdroid/release/*.apk ../info.zamojski.soft.towercollector_dev.apk;
./gradlew -stop;

cd ~/app_builds/homeapp;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../io.github.domi04151309.home_dev.apk;
./gradlew -stop;

cd ~/app_builds/revolutionirc;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../io.mrarm.irc_dev.apk;
./gradlew -stop;

cd ~/app_builds/phonesaver;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../link.standen.michael.phonesaver_dev.apk;
./gradlew -stop;

cd ~/app_builds/carreport;
echo $PWD;
echo 'build' $PWD >> ../build;
chmod u+x gradlew;
sed '/signingConfig signingConfigs.release/s/^/\/\//' -i app/build.gradle;
sed -i -e '/fullImplementation.* {/,/}/d' -e '/fullImplementation /d' app/build.gradle;
./gradlew assembleFossRelease;
mv app/build/outputs/apk/foss/release/app-foss-release-unsigned.apk ../me.kuehle.carreport_dev.apk;
./gradlew -stop;

cd ~/app_builds/shelter;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../net.typeblog.shelter_dev.apk;
./gradlew -stop;

cd ~/app_builds/foxydroid;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv build/outputs/apk/release/foxy*.apk ../nya.kitsunyan.foxydroid_dev.apk;
./gradlew -stop;

cd ~/app_builds/editor;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv build/outputs/apk/release/*.apk ../org.billthefarmer.editor_dev.apk;
./gradlew -stop;

cd ~/app_builds/opentasks;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv opentasks/build/outputs/apk/release/*.apk ../org.dmfs.tasks_dev.apk;
./gradlew -stop;

cd ~/app_builds/localgsm;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/*.apk ../org.fitchfamily.android.gsmlocation_dev.apk;
./gradlew -stop;

cd ~/app_builds/gitnex;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleFreeRelease;
mv app/build/outputs/apk/free/release/*.apk ../org.mian.gitnex_dev.apk;
./gradlew -stop;

cd ~/app_builds/mozillanlp;
echo $PWD;
echo 'build' $PWD >> ../build;
#add permission in src/main/AndroidManifest.xml
#<uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION"/>
#sed -i '/FINE_LOCATION\" \/>/s/^/\/\//' 
./gradlew assembleRelease;
mv build/outputs/apk/release/*.apk ../org.microg.nlp.backend.ichnaea_dev.apk;
./gradlew -stop;

cd ~/app_builds/nominatim;
echo $PWD;
echo 'build' $PWD >> ../build;
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64;
./gradlew assembleRelease;
mv build/outputs/apk/*.apk ../org.microg.nlp.backend.nominatim_dev.apk;
./gradlew -stop;
JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64;

cd ~/app_builds/opentopomap;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../org.nitri.opentopo_dev.apk;
./gradlew -stop;

cd ~/app_builds/newpipe;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/*.apk ../org.schabi.newpipe_dev.apk;
./gradlew -stop;

cd ~/app_builds/openkeychain;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleFdroidRelease;
mv OpenKeychain/build/outputs/apk/fdroid/release/*.apk ../org.sufficientlysecure.keychain_dev.apk;
./gradlew -stop;

cd ~/app_builds/termbot;
echo $PWD;
echo 'build' $PWD >> ../build;
sed -i -e '/googleImplementation/d' app/build.gradle;
sed -i -e 's/21.3.6528147/21.4.7075529/' app/build.gradle;
sed -i -e 's|url MAVEN_REPO_CACHE|url "https://jitpack.io"|' build.gradle;
sed -i -e 's|url MAVEN_REPO_CACHE|url "https://jitpack.io"|' settings.gradle;
./gradlew assembleOssRelease;
mv app/build/outputs/apk/oss/release/*.apk ../org.sufficientlysecure.termbot_dev.apk;
./gradlew -stop;

cd ~/app_builds/superfreeze;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv build/outputs/apk/release/*.apk ../superfreeze.tool.android_dev.apk;
./gradlew -stop;

cd ~/app_builds/etar;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv build/outputs/apk/release/*.apk ../ws.xsoh.etar_dev.apk;
./gradlew -stop;

cd ~/app_builds/grocy;
echo $PWD;
echo 'build' $PWD >> ../build;
./gradlew assembleRelease;
mv app/build/outputs/apk/release/app-release-unsigned.apk ../xyz.zedler.patrick.grocy_dev.apk;
./gradlew -stop;

################
#microg section#
################

###no apk output?###
cd ~/app_builds/unifiednlp;
echo $PWD;
echo 'build' $PWD >> ../build;
find . -type f -print0 -name build.gradle | xargs -0 sed -i -e '/com.github.dcendents/d';
./gradlew assemble;
mv app/build/outputs/apk/NetworkLocation/release/*.apk ../org.microg.nlp_dev.apk;
./gradlew -stop;

cd ~/app_builds/gmscore;
echo $PWD;
echo 'build' $PWD >> ../build;
#./gradlew assembleWithMapboxWithNearbyRelease;
#mv play-services-core/build/outputs/apk/withMapboxWithNearby/release/*.apk ../com.google.android.gms_dev.apk;
./gradlew assembleWithVtmWithNearbyRelease;
mv play-services-core/build/outputs/apk/withVtmWithNearby/release/*.apk ../com.google.android.gms_dev.apk;
./gradlew -stop;

mv ~/app_builds/*.apk ~/app_builds/fdroid/unsigned/;

################
#fdroid updates#
################

cd ~/app_builds/_fdroidserver;
git pull;
#cd ~/app_builds/fdroid;
#git pull origin master;

date;
echo END BUILDING;

sudo shutdown -h now;