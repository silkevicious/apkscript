#!/bin/bash
date;
echo START;

###############
#check updates#
###############
cd ~;
sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt autoremove --purge -y && sudo apt clean;
./app_builds/android_sdk/cmdline-tools/latest/bin/sdkmanager --update;

#######
#reset#
#######

cd ~/app_builds/fedilab;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/nitterizeme;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/davx5;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/organicmaps;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/vanilla;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/gpstest;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/aurorastore;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/smartcookieweb;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/ctemplar;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/librespeed/;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/fediphoto/;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/subz;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/k9mail;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/syncthing;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/skymap;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/pdfviewer;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/scrambledexif;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/vanillatag;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/vanillacover;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

###token genius error
#cd ~/app_builds/vanillalyrics;
#echo $PWD;
#git reset --hard --recurse-submodules;
#git pull --recurse-submodules;

cd ~/app_builds/vanillameta;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/tusky;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/keepassdx;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/nextcloud;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/seadroid;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/qrscanner;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

#cd ~/app_builds/spotiflyer;
#echo $PWD;
#git reset --hard --recurse-submodules;
#git pull --recurse-submodules;

cd ~/app_builds/termux;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/termuxapi;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/wireguard;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/corona;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/opentracks;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/imagepipe;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/acrylic;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/blabberim;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/osmdashboard;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/tutanota;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/streetcomplete;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/florisboard;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/netguard;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/towercollector;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/homeapp;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/revolutionirc;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/nextcloudnotes;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/phonesaver;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/carreport;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/shelter;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/foxydroid;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/openfoodfacts;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/editor;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/paintroid;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/opentasks;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/localgsm;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/gitnex;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/mozillanlp;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/nominatim;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/opentopomap;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/newpipe;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/nextcloudbookmarks;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/openkeychain;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/termbot;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/dvd;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/invizible;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/superfreeze;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/etar;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/grocy;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

################
#microg section#
################

###no apk output?###
cd ~/app_builds/unifiednlp;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/gmscore;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/gsfproxy;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/fakestore;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

cd ~/app_builds/droidguard;
echo $PWD;
git reset --hard --recurse-submodules;
git pull --recurse-submodules;

################
#fdroid updates#
################

#pip3 install --upgrade fdroidserver;
cd ~/app_builds/_fdroidserver;
git pull;
#cd ~/app_builds/fdroid;
#git pull origin master;

date;
echo END UPDATE;